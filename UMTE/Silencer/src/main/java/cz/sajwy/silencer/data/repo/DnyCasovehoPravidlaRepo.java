package cz.sajwy.silencer.data.repo;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import cz.sajwy.silencer.data.DBManager;
import cz.sajwy.silencer.data.model.Den;

public class DnyCasovehoPravidlaRepo {
    private static final String TABLE = "DnyCasovehoPravidla";

    private static final String KEY_FK_PRAVIDLO = "ID_pravidlo";
    private static final String KEY_FK_DEN = "ID_den";

    private static final String[] COLUMNS = {KEY_FK_PRAVIDLO, KEY_FK_DEN};

    public DnyCasovehoPravidlaRepo() {
    }

    public static String createTable(){
        return "CREATE TABLE " + TABLE  +
                "(" +
                    KEY_FK_PRAVIDLO  + " INTEGER REFERENCES CasovePravidlo," +
                    KEY_FK_DEN + " INTEGER REFERENCES Den " +
                ");";
    }

    public static String createIndex() {
        return "CREATE INDEX ix_id_den ON " + TABLE + "(" + KEY_FK_DEN + ");";
    }

    public static void insert(int idPravidla, ArrayList<Den> dnyPravidla) {
        SQLiteDatabase db = DBManager.getInstance().openDatabase();

        String query = "";
        for(int i = 0;i < dnyPravidla.size();i++) {
            query = "INSERT INTO " + TABLE + " (" + KEY_FK_PRAVIDLO + ", " + KEY_FK_DEN + ") " +
                    "VALUES " + "(" + idPravidla + " ,'" + dnyPravidla.get(i).getId_den() + "');";

            db.execSQL(query);
        }

        DBManager.getInstance().closeDatabase();
    }

    public static void update(int idPravidla, ArrayList<Den> dnyPravidla) {
        delete(idPravidla);
        insert(idPravidla, dnyPravidla);
    }

    public static void delete(int idPravidla) {
        SQLiteDatabase db = DBManager.getInstance().openDatabase();

        String query = "DELETE FROM " + TABLE + " WHERE " + KEY_FK_PRAVIDLO + " = " + idPravidla;
        db.execSQL(query);

        DBManager.getInstance().closeDatabase();
    }

    public static ArrayList<Integer> getIdDnu(int idPravidla) {
        SQLiteDatabase db = DBManager.getInstance().openDatabase();

        String query = "SELECT " + KEY_FK_DEN + " FROM " + TABLE +
                        " WHERE " + KEY_FK_PRAVIDLO + " = " + idPravidla +
                        " ORDER BY " + KEY_FK_DEN;
        Cursor cursor = db.rawQuery(query, null);
        ArrayList<Integer> idDnu = new ArrayList<>();

        if (cursor.moveToFirst()) {
            do {
                int id = cursor.getInt(cursor.getColumnIndex(KEY_FK_DEN));

                idDnu.add(id);
            } while (cursor.moveToNext());
        }

        cursor.close();
        DBManager.getInstance().closeDatabase();

        return idDnu;
    }

}
