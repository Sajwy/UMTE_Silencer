package cz.sajwy.silencer.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import java.util.Calendar;

import cz.sajwy.silencer.utils.Utils;
import cz.sajwy.silencer.data.repo.CasovaceRepo;

public class BootReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if(CasovaceRepo.get() == 1) {
            Utils.zrusCasovace(context, false);
            Utils.nastavCasovace(context, Calendar.getInstance());
        }
    }
}
