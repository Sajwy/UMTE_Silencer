package cz.sajwy.silencer.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

import cz.sajwy.silencer.R;
import cz.sajwy.silencer.data.repo.KategorieRepo;

import static android.widget.AdapterView.*;

public class KategoriePravidelActivity extends AppCompatActivity {

    private ListView lvKategorie;
    private ArrayAdapter<String> adapter;
    private ArrayList<String> kategorie;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kategorie_pravidel);

        lvKategorie = (ListView) findViewById(R.id.lvKategoriePravidel);
        kategorie = KategorieRepo.getAllKategorieNazvy();
        adapter = new ArrayAdapter<>(this, R.layout.lv_kategorie_pravidel_radek,R.id.tvRadek,kategorie);
        lvKategorie.setAdapter(adapter);
        lvKategorie.setOnItemClickListener(itemClickListener);
    }

    OnItemClickListener itemClickListener = new OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            String nazevKategorie = lvKategorie.getItemAtPosition(position).toString().trim();

            Intent intent = new Intent(getApplicationContext(), NovePravidloActivity.class);
            intent.putExtra("kategorie", nazevKategorie);

            startActivity(intent);
        }
    };
}
