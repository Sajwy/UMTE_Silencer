package cz.sajwy.silencer.data.repo;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import cz.sajwy.silencer.data.DBManager;
import cz.sajwy.silencer.data.model.Pravidlo;
import cz.sajwy.silencer.data.model.WifiPravidlo;

public class WifiPravidloRepo {
    private static final String TABLE = "WifiPravidlo";

    private static final String KEY_FK_PRAVIDLO = "ID_pravidlo";
    private static final String KEY_WIFI = "Wifi";

    private static final String[] COLUMNS = {KEY_FK_PRAVIDLO, KEY_WIFI};

    public WifiPravidloRepo() {
    }

    public static String createTable(){
        return "CREATE TABLE " + TABLE  +
                "(" +
                    KEY_FK_PRAVIDLO  + " INTEGER REFERENCES Pravidlo," +
                    KEY_WIFI + " TEXT " +
                ");";
    }

    public static String createIndex() {
        return "CREATE INDEX ix_id_wp ON " + TABLE + "(" + KEY_FK_PRAVIDLO + ");"+
                "CREATE INDEX ix_wifi ON " + TABLE + "(" + KEY_WIFI + ");";
    }

    public static WifiPravidlo getWifiPravidlo(int id) {
        Pravidlo pravidlo = PravidloRepo.getPravidlo(id);

        SQLiteDatabase db = DBManager.getInstance().openDatabase();

        Cursor cursor = db.query(TABLE, // a. table
                COLUMNS, // b. column names
                KEY_FK_PRAVIDLO + " = ?", // c. selections
                new String[]{String.valueOf(id)}, // d. selections args
                null, // e. group by
                null, // f. having
                null, // g. order by
                null); // h. limit

        if (cursor != null)
            cursor.moveToFirst();

        WifiPravidlo wifiPravidlo = new WifiPravidlo(id,pravidlo.getNazev(),pravidlo.getStav(),
                pravidlo.getVibrace(),pravidlo.getKategorie(),cursor.getString(cursor.getColumnIndex(KEY_WIFI)));

        cursor.close();
        DBManager.getInstance().closeDatabase();

        return wifiPravidlo;
    }

    public static void insertWifiPravidlo(WifiPravidlo wifiPravidlo){
        int id = PravidloRepo.insertPravidlo(new Pravidlo(wifiPravidlo.getNazev(), wifiPravidlo.getStav(),
                wifiPravidlo.getVibrace(), wifiPravidlo.getKategorie()));

        SQLiteDatabase db = DBManager.getInstance().openDatabase();

        String query = "INSERT INTO " + TABLE + " (" + KEY_FK_PRAVIDLO + ", " + KEY_WIFI + ") " +
                "VALUES " + "(" + id + " ,'" + wifiPravidlo.getNazev_wifi() + "');";

        db.execSQL(query);

        DBManager.getInstance().closeDatabase();
    }

    public static void updateWifiPravidlo(WifiPravidlo novePravidlo) {
        SQLiteDatabase db = DBManager.getInstance().openDatabase();

        String query = "UPDATE " + TABLE + " SET " + KEY_WIFI + " = '" + novePravidlo.getNazev_wifi() +
                "' WHERE " + KEY_FK_PRAVIDLO + " = " + novePravidlo.getId_pravidlo();
        db.execSQL(query);

        DBManager.getInstance().closeDatabase();

        Pravidlo pravidlo = new Pravidlo(novePravidlo.getId_pravidlo(), novePravidlo.getNazev(), novePravidlo.getStav(),
                novePravidlo.getVibrace(),novePravidlo.getKategorie());
        PravidloRepo.updatePravidlo(pravidlo);
    }

    public static void deleteWifiPravidlo(int id) {
        SQLiteDatabase db = DBManager.getInstance().openDatabase();

        String query = "DELETE FROM " + TABLE + " WHERE " + KEY_FK_PRAVIDLO + " = " + id;
        db.execSQL(query);

        DBManager.getInstance().closeDatabase();

        PravidloRepo.deletePravidlo(id);
    }

    public static ArrayList<String> getWifiNazvy() {
        SQLiteDatabase db = DBManager.getInstance().openDatabase();

        String query = "SELECT " + KEY_WIFI + " FROM " + TABLE + " ORDER BY " + KEY_WIFI;
        Cursor cursor = db.rawQuery(query, null);
        ArrayList<String> wifiNazvy = new ArrayList<String>();

        if (cursor.moveToFirst()) {
            while (cursor.moveToNext()) {
                wifiNazvy.add(cursor.getString(cursor.getColumnIndex(KEY_WIFI)));
            }
        }

        cursor.close();
        DBManager.getInstance().closeDatabase();

        return wifiNazvy;
    }
}
