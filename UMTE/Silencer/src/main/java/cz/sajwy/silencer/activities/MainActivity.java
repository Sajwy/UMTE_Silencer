package cz.sajwy.silencer.activities;

import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Point;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.Calendar;

import cz.sajwy.silencer.R;
import cz.sajwy.silencer.utils.Utils;
import cz.sajwy.silencer.data.repo.CasovaceRepo;
import cz.sajwy.silencer.data.repo.PravidloRepo;

import static android.view.View.OnClickListener;

public class MainActivity extends AppCompatActivity {

    private Button btnNovePravidlo, btnSeznamPravidel, btnSluzba, btnOAplikaci, btnExit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Point size = new Point();
        getWindowManager().getDefaultDisplay().getSize(size);
        int width = size.x;
        int height = size.y;

        btnNovePravidlo = (Button) findViewById(R.id.btnNovePravidlo);
        btnSeznamPravidel = (Button) findViewById(R.id.btnSeznamPravidel);
        btnSluzba = (Button) findViewById(R.id.btnZapnoutVypnoutSluzbu);
        btnOAplikaci = (Button) findViewById(R.id.btnOAplikaci);
        btnExit = (Button) findViewById(R.id.btnExit);

        if(CasovaceRepo.get() == 1) {
            btnSluzba.setText(R.string.vypnout_sluzbu);//Vypnout službu
        } else {
            btnSluzba.setText(R.string.zapnout_sluzbu);//"Zapnout službu"
        }

        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT){
            btnNovePravidlo.setWidth(2 * width / 3);
            btnSeznamPravidel.setWidth(2 * width / 3);
            btnSluzba.setWidth(2 * width / 3);
            btnOAplikaci.setWidth(2 * width / 3);
            btnExit.setWidth(2 * width / 3);
        } else {
            btnNovePravidlo.setWidth(width / 2);
            //btnNovePravidlo.setHeight(height / 4);

            btnSeznamPravidel.setWidth(width / 2);
            //btnSeznamPravidel.setHeight(height / 4);

            btnSluzba.setWidth(width / 2);
            //btnSluzba.setHeight(height / 4);

            btnOAplikaci.setWidth(width / 2);
            //btnOAplikaci.setHeight(height / 4);

            btnExit.setWidth(width / 2);
            //btnExit.setHeight(height / 4);
        }

        btnNovePravidlo.setOnClickListener(buttonsListener);
        btnSeznamPravidel.setOnClickListener(buttonsListener);
        btnSluzba.setOnClickListener(buttonsListener);
        btnOAplikaci.setOnClickListener(buttonsListener);
        btnExit.setOnClickListener(buttonsListener);
    }

    final OnClickListener buttonsListener = new OnClickListener() {
        public void onClick(final View v) {
            switch(v.getId()) {
                case R.id.btnNovePravidlo:
                    startActivity(new Intent(getApplicationContext(), KategoriePravidelActivity.class));
                    break;
                case R.id.btnSeznamPravidel:
                    startActivity(new Intent(getApplicationContext(), SeznamPravidelActivity.class));
                    break;
                case R.id.btnZapnoutVypnoutSluzbu:
                    if(PravidloRepo.existujiPravidla()) {
                        if (CasovaceRepo.get() == 1) {
                            Utils.zrusCasovace(getApplicationContext(), false);
                            AudioManager audioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
                            audioManager.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
                            btnSluzba.setText(R.string.zapnout_sluzbu);
                        } else {
                            Utils.zrusCasovace(getApplicationContext(), false);
                            Utils.nastavCasovace(getApplicationContext(), Calendar.getInstance());
                            btnSluzba.setText(R.string.vypnout_sluzbu);
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), R.string.neexistencePravidel, Toast.LENGTH_SHORT).show();
                    }
                    break;
                case R.id.btnOAplikaci:
                    startActivity(new Intent(getApplicationContext(), OAplikaciActivity.class));
                    break;
                case R.id.btnExit:
                    finish();
                    break;
            }
        }
    };

    private Boolean ukoncit = false;
    @Override
    public void onBackPressed() {
        if (ukoncit) {
            finish();
        } else {
            Toast.makeText(this, R.string.ukonceniAplikace, Toast.LENGTH_SHORT).show();
            ukoncit = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    ukoncit = false;
                }
            }, 3 * 1000);
        }
    }
}
