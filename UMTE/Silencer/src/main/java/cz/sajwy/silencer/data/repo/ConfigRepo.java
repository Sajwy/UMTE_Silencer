package cz.sajwy.silencer.data.repo;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import cz.sajwy.silencer.data.DBManager;

/**
 * Created by Sajwy on 01.06.2017.
 */
public class ConfigRepo {
    private static final String TABLE = "Config";

    private static final String KEY_VYKONAVA_SE_PRAVIDLO = "VykonavaSePravidlo";

    private static final String[] COLUMNS = {KEY_VYKONAVA_SE_PRAVIDLO};

    public static String createTable(){
        return "CREATE TABLE " + TABLE  +
                "(" +
                    KEY_VYKONAVA_SE_PRAVIDLO  + " INTEGER" +
                ");";
    }

    public static String insertData(){
        return  "INSERT INTO " + TABLE + " (" + KEY_VYKONAVA_SE_PRAVIDLO + ") VALUES (0);";
    }

    public static void update(int vykonavaSePravidlo) {
        SQLiteDatabase db = DBManager.getInstance().openDatabase();

        String query = "UPDATE " + TABLE + " SET " + KEY_VYKONAVA_SE_PRAVIDLO + " = " + vykonavaSePravidlo;

        db.execSQL(query);

        DBManager.getInstance().closeDatabase();
    }

    public static int get() {
        SQLiteDatabase db = DBManager.getInstance().openDatabase();

        String query = "SELECT " + KEY_VYKONAVA_SE_PRAVIDLO + " FROM " + TABLE;

        Cursor cursor = db.rawQuery(query, null);

        if (cursor != null)
            cursor.moveToFirst();

        int vykonavaSe = cursor.getInt(cursor.getColumnIndex(KEY_VYKONAVA_SE_PRAVIDLO));

        cursor.close();
        DBManager.getInstance().closeDatabase();

        return vykonavaSe;
    }
}
