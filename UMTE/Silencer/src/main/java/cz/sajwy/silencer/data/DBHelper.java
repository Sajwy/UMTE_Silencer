package cz.sajwy.silencer.data;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import cz.sajwy.silencer.app.App;
import cz.sajwy.silencer.data.model.CasovePravidlo;
import cz.sajwy.silencer.data.model.Den;
import cz.sajwy.silencer.data.model.KalendarovePravidlo;
import cz.sajwy.silencer.data.model.Kategorie;
import cz.sajwy.silencer.data.model.PolohovePravidlo;
import cz.sajwy.silencer.data.model.Pravidlo;
import cz.sajwy.silencer.data.model.WifiPravidlo;
import cz.sajwy.silencer.data.repo.CasovaceRepo;
import cz.sajwy.silencer.data.repo.CasovePravidloRepo;
import cz.sajwy.silencer.data.repo.ConfigRepo;
import cz.sajwy.silencer.data.repo.DenRepo;
import cz.sajwy.silencer.data.repo.DnyCasovehoPravidlaRepo;
import cz.sajwy.silencer.data.repo.IntentRepo;
import cz.sajwy.silencer.data.repo.KalendarovePravidloRepo;
import cz.sajwy.silencer.data.repo.KategorieRepo;
import cz.sajwy.silencer.data.repo.PolohovePravidloRepo;
import cz.sajwy.silencer.data.repo.PravidloRepo;
import cz.sajwy.silencer.data.repo.WifiPravidloRepo;

public class DBHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "MuteManager";
    private static final int DATABASE_VERSION = 1;

    public DBHelper() {
        super(App.getContext(), DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(KategorieRepo.createTable());
        db.execSQL(KategorieRepo.insertData());
        db.execSQL(PravidloRepo.createTable());
        db.execSQL(DenRepo.createTable());
        db.execSQL(DenRepo.insertData());
        db.execSQL(KalendarovePravidloRepo.createTable());
        db.execSQL(WifiPravidloRepo.createTable());
        db.execSQL(PolohovePravidloRepo.createTable());
        db.execSQL(CasovePravidloRepo.createTable());
        db.execSQL(DnyCasovehoPravidlaRepo.createTable());
        db.execSQL(ConfigRepo.createTable());
        db.execSQL(ConfigRepo.insertData());
        db.execSQL(IntentRepo.createTable());
        db.execSQL(CasovaceRepo.createTable());
        db.execSQL(CasovaceRepo.insertData());
        db.execSQL(DnyCasovehoPravidlaRepo.createIndex());
        db.execSQL(CasovePravidloRepo.createIndex());
        db.execSQL(DenRepo.createIndex());
        db.execSQL(KalendarovePravidloRepo.createIndex());
        db.execSQL(KategorieRepo.createIndex());
        db.execSQL(PravidloRepo.createIndex());
        db.execSQL(WifiPravidloRepo.createIndex());
        db.execSQL(PolohovePravidloRepo.createIndex());
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + Kategorie.class.getSimpleName());
        db.execSQL("DROP TABLE IF EXISTS " + Pravidlo.class.getSimpleName());
        db.execSQL("DROP TABLE IF EXISTS " + Den.class.getSimpleName());
        db.execSQL("DROP TABLE IF EXISTS " + KalendarovePravidlo.class.getSimpleName());
        db.execSQL("DROP TABLE IF EXISTS " + WifiPravidlo.class.getSimpleName());
        db.execSQL("DROP TABLE IF EXISTS " + PolohovePravidlo.class.getSimpleName());
        db.execSQL("DROP TABLE IF EXISTS " + CasovePravidlo.class.getSimpleName());
        db.execSQL("DROP TABLE IF EXISTS " + "DnyCasovehoPravidla");
        db.execSQL("DROP TABLE IF EXISTS " + "Config");
        db.execSQL("DROP TABLE IF EXISTS " + "Intent");
        db.execSQL("DROP TABLE IF EXISTS " + "Casovace");
        onCreate(db);
    }
}
