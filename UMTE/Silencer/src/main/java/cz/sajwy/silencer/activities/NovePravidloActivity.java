package cz.sajwy.silencer.activities;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NavUtils;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import cz.sajwy.silencer.R;
import cz.sajwy.silencer.activities.adapters.LVAdapter;
import cz.sajwy.silencer.data.repo.CasovaceRepo;
import cz.sajwy.silencer.utils.Utils;
import cz.sajwy.silencer.data.model.CasovePravidlo;
import cz.sajwy.silencer.data.model.Den;
import cz.sajwy.silencer.data.model.KalendarovePravidlo;
import cz.sajwy.silencer.data.model.PolohovePravidlo;
import cz.sajwy.silencer.data.model.Pravidlo;
import cz.sajwy.silencer.data.model.WifiPravidlo;
import cz.sajwy.silencer.data.repo.CasovePravidloRepo;
import cz.sajwy.silencer.data.repo.DenRepo;
import cz.sajwy.silencer.data.repo.KalendarovePravidloRepo;
import cz.sajwy.silencer.data.repo.KategorieRepo;
import cz.sajwy.silencer.data.repo.PolohovePravidloRepo;
import cz.sajwy.silencer.data.repo.WifiPravidloRepo;

import static android.widget.AdapterView.OnItemClickListener;

public class NovePravidloActivity extends AppCompatActivity {

    ListView lvNovePravidlo;
    Pravidlo pravidlo;
    CasovePravidlo casovePravidlo;
    KalendarovePravidlo kalendarovePravidlo;
    WifiPravidlo wifiPravidlo;
    PolohovePravidlo polohovePravidlo;
    ArrayList<String> parametryVse;
    LVAdapter adapter;
    TextView tvNadpis, tvObsah, tvSwitchNadpis, tvSwitchObsah, tvPoloha, tvLat, tvLon;
    Switch prepinac;
    AlertDialog.Builder builder;
    AlertDialog alertDialog;
    boolean jeCasOdNastaven;
    String kategorie;
    WifiManager wifiManager;
    boolean boolVlastnorucne;
    boolean wifiScanPovolen;

    ArrayList<Integer> vybraneDnyBackup;
    ArrayList<Den> dny;

    String nazev = "";
    int stav = 1;
    int vibrace = 1;
    ArrayList<Integer> vybraneDny;
    String cas_od = "";
    String doba_trvani = "";
    String nazev_wifi = "";
    double latitude = Double.MIN_VALUE;
    double longitude = Double.MIN_VALUE;
    int radius = 0;
    String udalost = "";
    boolean lze = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nove_pravidlo);

        Intent intent = this.getIntent();
        kategorie = intent.getStringExtra("kategorie");

        setTitle("Nové " + kategorie.toLowerCase());

        switch (kategorie) {
            case "Časové pravidlo":
                pravidlo = new CasovePravidlo();
                jeCasOdNastaven = false;
                vybraneDny = new ArrayList<>();
                vybraneDnyBackup = new ArrayList<>();
                dny = DenRepo.getDny();
                break;
            case "Kalendářové pravidlo":
                pravidlo = new KalendarovePravidlo();
                break;
            case "Polohové pravidlo":
                pravidlo = new PolohovePravidlo();
                break;
            case "Wifi pravidlo":
                pravidlo = new WifiPravidlo();
                boolVlastnorucne = false;
                break;
            default:
                break;
        }

        pravidlo.setKategorie(KategorieRepo.getKategorieByNazev(kategorie));

        parametryVse = Utils.zjistiParametryPravidla(pravidlo, kategorie);

        lvNovePravidlo = (ListView) findViewById(R.id.lvNovePravidlo);
        adapter = new LVAdapter(getApplicationContext(), R.layout.lv_nove_pravidlo_tv, parametryVse, 0);
        lvNovePravidlo.setAdapter(adapter);
        lvNovePravidlo.setOnItemClickListener(itemClickListener);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_nove_pravidlo, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.action_save) {
            ulozPravidlo();
        } else if(item.getItemId() == android.R.id.home)
            NavUtils.navigateUpFromSameTask(this);
        return true;
    }

    private void ulozPravidlo() {
        String validace = zvalidujVstupy();

        if(!validace.equals("") || lze == false) {
            if(lze == false && !validace.equals("")) {
                validace = "Nevyplněn parametr: " + validace.substring(0, validace.length() - 2) + "!!!\n\n";
                validace += "Název události je již použit!!!";
            } else if(lze == false)
                validace += "Název události je již použit!!!";
            else
                validace = "Nevyplněn parametr: " + validace.substring(0, validace.length() - 2) + "!!!";
            Toast.makeText(getApplicationContext(), validace, Toast.LENGTH_LONG).show();
        } else {
            switch (kategorie) {
                case "Časové pravidlo":
                    CasovePravidloRepo.insertCasovePravidlo(casovePravidlo);
                    if(CasovaceRepo.get() == 1) {
                        long[] casoveUdaje = Utils.prevedTimeStringyNaMilisekundy(casovePravidlo.getCas_od(), casovePravidlo.getDoba_trvani(), false);
                        long nyni = Calendar.getInstance().getTimeInMillis();
                        ArrayList<Den> dny = casovePravidlo.getDny();
                        Den den = Utils.zjistiAktualniDen();
                        if (Utils.jeDenObsazen(dny, den) && casoveUdaje[1] > nyni && casovePravidlo.getStav() == 1) {
                            Utils.zrusCasovace(getApplicationContext(), false);
                            Utils.nastavCasovace(getApplicationContext(), Calendar.getInstance());
                        }
                    }
                    break;
                case "Kalendářové pravidlo":
                    KalendarovePravidloRepo.insertKalendarovePravidlo(kalendarovePravidlo);
                    if(CasovaceRepo.get() == 1) {
                        if (Utils.jeUdalostDnes(getApplicationContext(), kalendarovePravidlo.getUdalost()) && kalendarovePravidlo.getStav() == 1) {
                            Utils.zrusCasovace(getApplicationContext(), false);
                            Utils.nastavCasovace(getApplicationContext(), Calendar.getInstance());
                        }
                    }
                    break;
                case "Polohové pravidlo":
                    PolohovePravidloRepo.insertPolohovePravidlo(polohovePravidlo);
                    break;
                case "Wifi pravidlo":
                    WifiPravidloRepo.insertWifiPravidlo(wifiPravidlo);
                    break;
            }
            Toast.makeText(getApplicationContext(), R.string.ulozeno, Toast.LENGTH_SHORT).show();
        }
    }

    private String zvalidujVstupy() {
        String validace = "";

        if(!nazev.equals(""))
            pravidlo.setNazev(nazev);
        else
            validace += "název pravidla, ";
        pravidlo.setStav(stav);
        pravidlo.setVibrace(vibrace);

        switch (kategorie) {
            case "Časové pravidlo":
                casovePravidlo = (CasovePravidlo) pravidlo;
                if(vybraneDny.size() != 0)
                    casovePravidlo.setDny(Utils.vratDnyPravidla(vybraneDny));
                else
                    validace += "den/dny opakování, ";
                if(!cas_od.equals(""))
                    casovePravidlo.setCas_od(cas_od);
                else
                    validace += "čas od, ";
                if(!doba_trvani.equals(""))
                    casovePravidlo.setDoba_trvani(doba_trvani);
                else
                    validace += "doba trvání, ";
                break;
            case "Kalendářové pravidlo":
                kalendarovePravidlo = (KalendarovePravidlo) pravidlo;
                lze = KalendarovePravidloRepo.lzeNazevUdalostiPouzit(udalost);
                if(!udalost.equals("")) {
                    if(lze)
                        kalendarovePravidlo.setUdalost(udalost);
                } else
                    validace += "událost, ";
                break;
            case "Polohové pravidlo":
                polohovePravidlo = (PolohovePravidlo) pravidlo;
                if(latitude != Double.MIN_VALUE & longitude != Double.MIN_VALUE) {
                    polohovePravidlo.setLatitude(latitude);
                    polohovePravidlo.setLongitude(longitude);
                } else
                    validace += "poloha, ";
                if(radius != 0)
                    polohovePravidlo.setRadius(radius);
                else
                    validace += "radius, ";
                break;
            case "Wifi pravidlo":
                wifiPravidlo = (WifiPravidlo) pravidlo;
                if(!nazev_wifi.equals(""))
                    wifiPravidlo.setNazev_wifi(nazev_wifi);
                else
                    validace += "název wifi, ";
                break;
        }

        return validace;
    }

    OnItemClickListener itemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            switch (parametryVse.get(position)) {
                case LVAdapter.NAZEV:
                case LVAdapter.UDALOST:
                    zobrazEditTextDialog(view, position);
                    break;
                case LVAdapter.RADIUS:
                    zobrazRadiusDialog(view);
                    break;
                case LVAdapter.CAS_OD:
                    zobrazCasOdDialog(view);
                    break;
                case LVAdapter.DOBA_TRVANI:
                    if(jeCasOdNastaven)
                        zobrazDobaTrvaniDialog(view);
                    else
                        Toast.makeText(getApplicationContext(),R.string.casOdPrvni,Toast.LENGTH_SHORT).show();
                    break;
                case LVAdapter.DNY:
                    zobrazDnyDialog(view);
                    break;
                case LVAdapter.VIBRACE:
                case LVAdapter.STAV:
                    zmenStavPrepinace(view);
                    break;
                case LVAdapter.POLOHA:
                    zjistiPolohu(view);
                    break;
                case LVAdapter.WIFI:
                    wifiManager = (WifiManager) getSystemService(WIFI_SERVICE);
                    if(wifiManager != null) {
                        if (wifiManager.isWifiEnabled() == false) {
                            wifiScanPovolen = false;
                            new WifiAsyncTask(view).execute();
                        } else {
                            zobrazWifiDialog(view);
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), R.string.wifiNepodporovano, Toast.LENGTH_LONG).show();
                    }
                    break;
            }
        }
    };

    private void zjistiPolohu(View view) {
        tvPoloha = (TextView) view.findViewById(R.id.tvPoloha);
        tvLat = (TextView) view.findViewById(R.id.tvLatitude);
        tvLon = (TextView) view.findViewById(R.id.tvLongitude);

        final ProgressDialog polohaProgressDialog = new ProgressDialog(NovePravidloActivity.this, R.style.DialogStyle);
        polohaProgressDialog.setTitle(R.string.vyckejte);
        polohaProgressDialog.setMessage("Zjišťuji polohu...");
        polohaProgressDialog.setCancelable(true);
        polohaProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        polohaProgressDialog.show();

        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        boolean isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        if (isNetworkEnabled) {
            ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetwork = connectivityManager.getActiveNetworkInfo();

            boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();

            if(isConnected) {
                Criteria criteria = new Criteria();
                criteria.setAccuracy(Criteria.ACCURACY_FINE);

                if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }

                locationManager.requestSingleUpdate(criteria, new LocationListener() {
                    @Override
                    public void onLocationChanged(Location location) {
                        tvLat.setText(location.getLatitude()+"");
                        tvLon.setText(location.getLongitude() + "");
                        latitude = location.getLatitude();
                        longitude = location.getLongitude();
                        polohaProgressDialog.dismiss();
                    }

                    @Override
                    public void onStatusChanged(String provider, int status, Bundle extras) {
                    }

                    @Override
                    public void onProviderEnabled(String provider) {
                    }

                    @Override
                    public void onProviderDisabled(String provider) {
                    }
                }, null);

            } else {
                polohaProgressDialog.dismiss();
                Toast.makeText(getApplicationContext(), R.string.zadnePripojeni, Toast.LENGTH_SHORT).show();
            }
        } else {
            polohaProgressDialog.dismiss();
            Toast.makeText(getApplicationContext(), R.string.noNetworkProvider, Toast.LENGTH_SHORT).show();
        }
    }

    private class WifiAsyncTask extends AsyncTask<Void, Void, Void> {
        private ProgressDialog wifiProgressDialog;
        private View view;

        public WifiAsyncTask(View view) {
            this.view = view;
        }

        @Override
        protected void onPreExecute() {
            wifiProgressDialog = new ProgressDialog(NovePravidloActivity.this, R.style.DialogStyle);
            wifiProgressDialog.setTitle(R.string.vyckejte);
            wifiProgressDialog.setMessage("Zapínám wifi a skenuji okolí...");
            wifiProgressDialog.setCancelable(true);
            wifiProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            wifiProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            wifiManager.setWifiEnabled(true);

            while (!wifiManager.isWifiEnabled()) {
                try {
                    Thread.sleep(200);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            int pocet = 0;
            while(!wifiScanPovolen & pocet < 25) {
                if(wifiManager.getScanResults().size() == 0) {
                    try {
                        pocet++;
                        Thread.sleep(200);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    wifiScanPovolen = true;
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            wifiProgressDialog.dismiss();
            zobrazWifiDialog(view);
        }
    }

    private void zobrazWifiDialog(View view) {
        tvNadpis = (TextView) view.findViewById(R.id.tvNadpis);
        tvObsah = (TextView) view.findViewById(R.id.tvObsah);

        LayoutInflater inflater = LayoutInflater.from(getApplicationContext());
        View mView = inflater.inflate(R.layout.wifi_dialog_box, null);

        builder = new AlertDialog.Builder(NovePravidloActivity.this, R.style.DialogStyle);
        builder.setView(mView);
        builder.setTitle(tvNadpis.getText());

        final EditText input = (EditText) mView.findViewById(R.id.etInput);

        final Spinner spinner = (Spinner) mView.findViewById(R.id.spinWifi);

        List<String> nazvyWifinList = Utils.vratNazvyWifinList(wifiManager);

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(this,R.layout.spinner_item,R.id.text1, nazvyWifinList);
        dataAdapter.setDropDownViewResource(R.layout.spinner_item);
        spinner.setAdapter(dataAdapter);

        final RadioButton rbWifiDaU = (RadioButton) mView.findViewById(R.id.rbWifiDostAUloz);
        final RadioButton rbWifiVlastni = (RadioButton) mView.findViewById(R.id.rbWifiVlastni);

        if(nazvyWifinList.isEmpty()) {
            boolVlastnorucne = true;
            rbWifiDaU.setEnabled(false);
        }

        rbWifiDaU.setChecked(!boolVlastnorucne);
        rbWifiVlastni.setChecked(boolVlastnorucne);

        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(v.getId() == R.id.rbWifiDostAUloz) {
                    rbWifiDaU.setChecked(true);
                    rbWifiVlastni.setChecked(false);
                    spinner.setEnabled(true);
                    input.setEnabled(false);
                    boolVlastnorucne = false;
                } else if(v.getId() == R.id.rbWifiVlastni) {
                    rbWifiDaU.setChecked(false);
                    rbWifiVlastni.setChecked(true);
                    spinner.setEnabled(false);
                    input.setEnabled(true);
                    boolVlastnorucne = true;
                }
            }
        };

        rbWifiDaU.setOnClickListener(onClickListener);
        rbWifiVlastni.setOnClickListener(onClickListener);

        if(boolVlastnorucne) {
            spinner.setEnabled(false);
            input.setEnabled(true);

            String prvniSlovo;
            try {
                prvniSlovo = tvObsah.getText().toString().substring(0,tvObsah.getText().toString().indexOf(" "));
            } catch (Exception e) {
                prvniSlovo = "";
            }

            if(prvniSlovo.equals("Vyplňte")){
                input.setHint(R.string.wifiObsah);
            } else {
                input.setText(tvObsah.getText());
                input.setSelection(input.getText().length());
            }
        } else {
            spinner.setEnabled(true);
            input.setEnabled(false);
            input.setHint(R.string.wifiObsah);

            String vyplnena_wifi = tvObsah.getText().toString();
            if(!vyplnena_wifi.equals(R.string.wifiObsah) & nazvyWifinList.contains(vyplnena_wifi))
                spinner.setSelection(nazvyWifinList.indexOf(vyplnena_wifi));
        }

        builder
                .setCancelable(false)
                .setPositiveButton(R.string.btnOk, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogBox, int id) {
                        if (boolVlastnorucne) {
                            if (input.getText().toString().length() != 0) {
                                tvObsah.setText(input.getText().toString());
                                nazev_wifi = tvObsah.getText().toString();
                            } else {
                                tvObsah.setText(R.string.wifiObsah);
                                nazev_wifi = "";
                            }
                        } else {
                            tvObsah.setText(spinner.getSelectedItem().toString());
                            nazev_wifi = tvObsah.getText().toString();
                        }
                    }
                })

                .setNegativeButton(R.string.btnZrusit,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogBox, int id) {
                                dialogBox.cancel();
                            }
                        });

        alertDialog = builder.create();
        alertDialog.show();

        alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(ResourcesCompat.getColor(getResources(), R.color.colorBlack, null));
    }

    private void zobrazEditTextDialog(View view, final int position) {
        tvNadpis = (TextView) view.findViewById(R.id.tvNadpis);
        tvObsah = (TextView) view.findViewById(R.id.tvObsah);

        LayoutInflater inflater = LayoutInflater.from(getApplicationContext());
        View mView = inflater.inflate(R.layout.edittext_dialog_box, null);

        builder = new AlertDialog.Builder(NovePravidloActivity.this, R.style.DialogStyle);
        builder.setView(mView);
        builder.setTitle(tvNadpis.getText());

        final EditText input = (EditText) mView.findViewById(R.id.etInput);

        String prvniSlovo;
        try {
            prvniSlovo = tvObsah.getText().toString().substring(0,tvObsah.getText().toString().indexOf(" "));
        } catch (Exception e) {
            prvniSlovo = "";
        }

        if(prvniSlovo.equals("Vyplňte") | prvniSlovo.equals("Zvolte")){
            input.setHint(tvObsah.getText());
        } else {
            input.setText(tvObsah.getText());
            input.setSelection(input.getText().length());
        }

        builder
                .setCancelable(false)
                .setPositiveButton(R.string.btnOk, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogBox, int id) {
                        tvObsah.setText(input.getText().toString());
                        switch (parametryVse.get(position)) {
                            case LVAdapter.NAZEV:
                                nazev = tvObsah.getText().toString();
                                break;
                            case LVAdapter.UDALOST:
                                udalost = tvObsah.getText().toString();
                                break;
                        }
                    }
                })

                .setNegativeButton(R.string.btnZrusit,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogBox, int id) {
                                dialogBox.cancel();
                            }
                        });

        alertDialog = builder.create();
        alertDialog.show();

        alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(ResourcesCompat.getColor(getResources(), R.color.colorBlack, null));
    }

    private void zobrazRadiusDialog(View view) {
        tvNadpis = (TextView) view.findViewById(R.id.tvNadpis);
        tvObsah = (TextView) view.findViewById(R.id.tvObsah);

        LayoutInflater inflater = LayoutInflater.from(getApplicationContext());
        View mView = inflater.inflate(R.layout.numberpicker_dialog_box, null);

        builder = new AlertDialog.Builder(NovePravidloActivity.this, R.style.DialogStyle);
        builder.setView(mView);
        builder.setTitle(tvNadpis.getText());

        final NumberPicker numberPicker = (NumberPicker) mView.findViewById(R.id.npRadius);
        numberPicker.setMaxValue(30);
        numberPicker.setMinValue(2);
        numberPicker.setWrapSelectorWheel(true);

        String[] pickerValues = new String[29];
        for (int i = 0; i < pickerValues.length; i++) {
            pickerValues[i] = (i+2)*5 + " m";
        }
        numberPicker.setDisplayedValues(pickerValues);

        String regexStr = "^[0-9]* m$";
        if(tvObsah.getText().toString().matches(regexStr))
        {
            String text = tvObsah.getText().toString();
            numberPicker.setValue(Integer.parseInt(text.substring(0, text.indexOf(" "))) / 5);
        }

        builder
                .setCancelable(false)
                .setPositiveButton(R.string.btnOk, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogBox, int id) {
                        tvObsah.setText(numberPicker.getValue() * 5 + " m");
                        radius = numberPicker.getValue() * 5;
                    }
                })

                .setNegativeButton(R.string.btnZrusit,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogBox, int id) {
                                dialogBox.cancel();
                            }
                        });

        alertDialog = builder.create();
        alertDialog.show();

        alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(ResourcesCompat.getColor(getResources(), R.color.colorBlack, null));
    }

    private void zobrazCasOdDialog(View view) {
        tvNadpis = (TextView) view.findViewById(R.id.tvNadpis);
        tvObsah = (TextView) view.findViewById(R.id.tvObsah);

        LayoutInflater inflater = LayoutInflater.from(getApplicationContext());
        View mView = inflater.inflate(R.layout.mytimepicker_dialog_box, null);

        builder = new AlertDialog.Builder(NovePravidloActivity.this, R.style.DialogStyle);
        builder.setView(mView);
        builder.setTitle(tvNadpis.getText());

        final NumberPicker npHod = (NumberPicker) mView.findViewById(R.id.npHod);
        npHod.setMaxValue(23);
        npHod.setMinValue(0);
        npHod.setWrapSelectorWheel(true);

        final String[] pickerValuesHod = new String[24];
        for (int i = 0; i < pickerValuesHod.length; i++) {
            pickerValuesHod[i] = Utils.pad(i);
        }
        npHod.setDisplayedValues(pickerValuesHod);

        final NumberPicker npMin = (NumberPicker) mView.findViewById(R.id.npMin);
        npMin.setMaxValue(59);
        npMin.setMinValue(0);
        npMin.setWrapSelectorWheel(true);

        String[] pickerValuesMin = new String[60];
        for (int i = 0; i < pickerValuesMin.length; i++) {
            pickerValuesMin[i] = Utils.pad(i);
        }
        npMin.setDisplayedValues(pickerValuesMin);

        String regexStr = "^(2[0-3]|[01]?[0-9]):([0-5]?[0-9])$";
        if(tvObsah.getText().toString().matches(regexStr))
        {
            String text = tvObsah.getText().toString();
            npHod.setValue(Integer.parseInt(Utils.unpad(text.substring(0, text.indexOf(":")))));
            npMin.setValue(Integer.parseInt(Utils.unpad(text.substring(text.indexOf(":") + 1, text.length()))));
        } else {
            Calendar cal = Calendar.getInstance();
            int hod = cal.get(Calendar.HOUR_OF_DAY);
            int min = cal.get(Calendar.MINUTE);

            npHod.setValue(hod);
            npMin.setValue(min);
        }

        builder
                .setCancelable(false)
                .setPositiveButton(R.string.btnOk, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogBox, int id) {
                        jeCasOdNastaven = true;
                        tvObsah.setText(Utils.pad(npHod.getValue()) + ":" + Utils.pad(npMin.getValue()));
                        cas_od = tvObsah.getText().toString();
                    }
                })

                .setNegativeButton(R.string.btnZrusit,
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogBox, int id) {
                            dialogBox.cancel();
                        }
                    });

        alertDialog = builder.create();
        alertDialog.show();

        alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(ResourcesCompat.getColor(getResources(), R.color.colorBlack, null));
    }

    private void zobrazDobaTrvaniDialog(View view) {
        tvNadpis = (TextView) view.findViewById(R.id.tvNadpis);
        tvObsah = (TextView) view.findViewById(R.id.tvObsah);

        LayoutInflater inflater = LayoutInflater.from(getApplicationContext());
        View mView = inflater.inflate(R.layout.mytimepicker_dialog_box2, null);

        builder = new AlertDialog.Builder(NovePravidloActivity.this, R.style.DialogStyle);
        builder.setView(mView);
        builder.setTitle(tvNadpis.getText());

        final NumberPicker npHod = (NumberPicker) mView.findViewById(R.id.npHod);
        npHod.setMaxValue(23);
        npHod.setMinValue(0);
        npHod.setWrapSelectorWheel(true);

        final String[] pickerValuesHod = new String[24];
        for (int i = 0; i < pickerValuesHod.length; i++) {
            pickerValuesHod[i] = Utils.pad(i);
        }
        npHod.setDisplayedValues(pickerValuesHod);

        final NumberPicker npMin = (NumberPicker) mView.findViewById(R.id.npMin);
        npMin.setMaxValue(59);
        npMin.setMinValue(0);
        npMin.setWrapSelectorWheel(true);

        String[] pickerValuesMin = new String[60];
        for (int i = 0; i < pickerValuesMin.length; i++) {
            pickerValuesMin[i] = Utils.pad(i);
        }
        npMin.setDisplayedValues(pickerValuesMin);

        if(npHod.getValue()==0 && npMin.getValue()==0)
            npMin.setValue(1);

        String regexStr = "^(2[0-3]|[01]?[0-9]):([0-5]?[0-9])$";
        if(tvObsah.getText().toString().matches(regexStr))
        {
            String text = tvObsah.getText().toString();
            npHod.setValue(Integer.parseInt(Utils.unpad(text.substring(0, text.indexOf(":")))));
            npMin.setValue(Integer.parseInt(Utils.unpad(text.substring(text.indexOf(":") + 1, text.length()))));
        }

        final TextView tvKonec = (TextView) mView.findViewById(R.id.tvKonec);

        npHod.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                if(npHod.getValue()==0 && npMin.getValue()==0)
                    npMin.setValue(1);
                tvKonec.setText("Konec v: " + Utils.dopocitejKonec(newVal, npMin.getValue(), cas_od));
            }
        });

        npMin.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                if(npHod.getValue()==0 && npMin.getValue()==0)
                    npMin.setValue(1);
                tvKonec.setText("Konec v: " + Utils.dopocitejKonec(npHod.getValue(), newVal, cas_od));
            }
        });

        builder
                .setCancelable(false)
                .setPositiveButton(R.string.btnOk, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogBox, int id) {
                        String cas = Utils.pad(npHod.getValue()) + ":" + Utils.pad(npMin.getValue());
                        tvObsah.setText(cas);
                        doba_trvani = cas;
                    }
                })

                .setNegativeButton(R.string.btnZrusit,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogBox, int id) {
                                dialogBox.cancel();
                            }
                        });

        alertDialog = builder.create();
        alertDialog.show();

        alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(ResourcesCompat.getColor(getResources(), R.color.colorBlack, null));
    }

    private void zobrazDnyDialog(View view) {
        tvNadpis = (TextView) view.findViewById(R.id.tvNadpis);
        tvObsah = (TextView) view.findViewById(R.id.tvObsah);

        builder = new AlertDialog.Builder(NovePravidloActivity.this, R.style.DialogStyle);
        builder.setTitle(tvNadpis.getText());

        final boolean[] isSelectedArray = {false,false,false,false,false,false,false};
        if(!vybraneDny.isEmpty()) {
            for(int i = 0;i < vybraneDny.size();i++) {
                isSelectedArray[vybraneDny.get(i)] = true;
            }
        }

        builder.setMultiChoiceItems(DenRepo.getNazvyDnuArray(), isSelectedArray, new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                if (isChecked) {
                    vybraneDny.add(which);
                } else {
                    vybraneDny.remove(Integer.valueOf(which));
                }
            }
        });

        builder
                .setCancelable(false)
                .setPositiveButton(R.string.btnOk, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogBox, int id) {

                        if (!vybraneDny.isEmpty()) {
                            Collections.sort(vybraneDny);
                            StringBuilder stringBuilder = new StringBuilder();

                            if (vybraneDny.size() == 7) {
                                stringBuilder = stringBuilder.append("Celý týden");
                            } else if (vybraneDny.size() == 5 & !vybraneDny.contains(5) & !vybraneDny.contains(6)) {
                                stringBuilder = stringBuilder.append("Pracovní dny");
                            } else if (vybraneDny.size() == 2 & vybraneDny.contains(5) & vybraneDny.contains(6)) {
                                stringBuilder = stringBuilder.append("Víkend");
                            } else if (vybraneDny.size() <= 4) {
                                for (int i = 0; i < vybraneDny.size(); i++) {
                                    String den = dny.get(vybraneDny.get(i)).getNazev();
                                    if(i < vybraneDny.size() - 1)
                                        stringBuilder = stringBuilder.append(den + ", ");
                                    else
                                        stringBuilder = stringBuilder.append(den);
                                }
                            } else {
                                for (int i = 0; i < vybraneDny.size(); i++) {
                                    String den = dny.get(vybraneDny.get(i)).getZkratka();
                                    if(i < vybraneDny.size() - 1)
                                        stringBuilder = stringBuilder.append(den + ", ");
                                    else
                                        stringBuilder = stringBuilder.append(den);
                                }
                            }

                            tvObsah.setText(stringBuilder.toString());

                            vybraneDnyBackup = (ArrayList<Integer>) vybraneDny.clone();
                        } else {
                            tvObsah.setText(R.string.denObsah);
                        }

                    }
                })

                .setNegativeButton(R.string.btnZrusit,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogBox, int id) {
                                vybraneDny = (ArrayList<Integer>) vybraneDnyBackup.clone();
                                dialogBox.cancel();
                            }
                        })

                .setNeutralButton(R.string.btnOznacitVse, null);

        alertDialog = builder.create();
        alertDialog.show();

        alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(ResourcesCompat.getColor(getResources(), R.color.colorBlack, null));
        alertDialog.getButton(DialogInterface.BUTTON_NEUTRAL).setTextColor(ResourcesCompat.getColor(getResources(), R.color.colorBlack, null));
        alertDialog.getButton(DialogInterface.BUTTON_NEUTRAL).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vybraneDny.clear();

                for (int i = 0; i < isSelectedArray.length; i++) {
                    alertDialog.getListView().setItemChecked(i, true);
                    vybraneDny.add(i);
                }
            }
        });
    }

    private void zmenStavPrepinace(View view) {
        tvSwitchNadpis = (TextView) view.findViewById(R.id.tvSwitchNadpis);
        tvSwitchObsah = (TextView) view.findViewById(R.id.tvSwitchObsah);
        prepinac = (Switch) view.findViewById(R.id.swID);

        if(tvSwitchNadpis.getText().toString().toLowerCase().equals("vibrace")) {
            if(prepinac.isChecked()) {
                prepinac.setChecked(false);
                tvSwitchObsah.setText(R.string.ne);
                vibrace = 0;
            } else {
                prepinac.setChecked(true);
                tvSwitchObsah.setText(R.string.ano);
                vibrace = 1;
            }
        } else if(tvSwitchNadpis.getText().toString().toLowerCase().equals("stav")) {
            if(prepinac.isChecked()) {
                prepinac.setChecked(false);
                tvSwitchObsah.setText(R.string.neaktiv);
                stav = 0;
            } else {
                prepinac.setChecked(true);
                tvSwitchObsah.setText(R.string.aktiv);
                stav = 1;
            }
        }
    }
}
